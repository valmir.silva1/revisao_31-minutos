#!/bin/bash

# Solicita ao usuário para inserir os nomes dos arquivos
read -p "Digite o nome do primeiro arquivo: " arquivo1
read -p "Digite o nome do segundo arquivo: " arquivo2
read -p "Digite o nome do terceiro arquivo: " arquivo3
read -p "Digite o nome do quarto arquivo: " arquivo4
read -p "Digite o nome do quinto arquivo: " arquivo5

# Inicializa contadores
existentes=0
nao_existentes=0

# Verifica a existência dos arquivos
for arquivo in "$arquivo1" "$arquivo2" "$arquivo3" "$arquivo4" "$arquivo5"; do
    if [ -e "$arquivo" ]; then
        existentes=$((existentes + 1))
    else
        nao_existentes=$((nao_existentes + 1))
    fi
done

# Imprime o resultado
echo "Quantidade de arquivos existentes: $existentes"
echo "Quantidade de arquivos não existentes: $nao_existentes"

