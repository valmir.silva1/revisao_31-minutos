#!/bin/bash

# Gera um número aleatório entre 2 e 13
gerar_numero() {
    echo $((RANDOM % 12 + 2))
}

# Cria os arquivos
for i in {1..3}; do
    numero=$(gerar_numero)
    nome_arquivo="${numero}.txt"

    if [ -e "$nome_arquivo" ]; then
        echo -e "\033[0;31mErro: O arquivo $nome_arquivo já existe.\033[0m"
        exit 1
    fi

    touch "$nome_arquivo"
    echo "Arquivo $nome_arquivo criado."
done

