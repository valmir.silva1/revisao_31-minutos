#!/bin/bash

# Cria os arquivos
for i in {1..3}; do
    numero=$((RANDOM % 12 + 2))
    nome_arquivo="${numero}.txt"
    touch "$nome_arquivo"
    echo "Arquivo $nome_arquivo criado."
done
